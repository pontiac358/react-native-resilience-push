
# react-native-resilience-push

## Установка

`$ npm install react-native-resilience-push@git@bitbucket.org:pontiac358/react-native-resilience-push.git --save`


#### iOS

1. Добавить в Podfile `pod 'react-native-resilience-push', :path => '../node_modules/react-native-resilience-push'`
2. Выполнить команду `pod install`

3. Добавить в AppDelegate:
```
#import <react-native-resilience-push/RNRPushManager.h>

@implementation AppDelegate {
  RNRPushManager *pushManager;
}

// В метод didFinishLaunchingWithOptions:
pushManager = [RNRPushManager allocWithZone:nil];
[pushManager setBridge:rootView.bridge];
[pushManager registerForPushNotificationsWithApplication:application withYandexAPiKey:@"----"];
  
// добавить методы
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
  [pushManager didRegisterUserNotificationSettings:notificationSettings];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  [pushManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  [pushManager didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  [pushManager didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
```
#### Android



## Usage
```
import {NativeEventEmitter, NativeModules} from 'react-native';
const { RNRPushManager } = NativeModules;
const pushManagerEmitter = new NativeEventEmitter(RNRPushManager);

const subscription1 = pushManagerEmitter.addListener(
	'PNFirebaseDeviceTokenUpdated',
	(event) => console.log("PNFirebaseDeviceTokenUpdated: " + event)
);
const subscription = pushManagerEmitter.addListener(
 	'PNDeviceTokenUpdated',
	(event) => console.log("PNDeviceTokenUpdated: " + event)
);
const subscription1 = pushManagerEmitter.addListener(
	'PNMessage',
	(event) => console.log("PNMessage: " + event)
);
```
