//
//  RNRMMapViewManager.h
//
//  Created by Resilience on 12.12.2019.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

@interface RNRPushManager : RCTEventEmitter <RCTBridgeModule>
- (void)registerForPushNotificationsWithApplication:(UIApplication *)application withYandexAPiKey:(NSString *)apiKey;
- (void)didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;
- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
@end

NS_ASSUME_NONNULL_END
