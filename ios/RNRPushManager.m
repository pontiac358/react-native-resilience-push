//
//  RNRMMapViewManager.m
//
//  Created by Resilience on 12.12.2019.
//

#import <React/RCTBridge.h>
#import "RNRPushManager.h"
#import <Firebase/Firebase.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <UserNotifications/UserNotifications.h>
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <YandexMobileMetricaPush/YandexMobileMetricaPush.h>

static NSString *const nRNRPushManagerAddListener = @"RNRPushManagerAddListener";
static NSString *const nRNRPNDeviceTokenUpdated = @"PNDeviceTokenUpdated";
static NSString *const nRNRPNFirebaseDeviceTokenUpdated = @"PNFirebaseDeviceTokenUpdated";
static NSString *const nRNRPNMessage = @"PNMessage";

@implementation RNRPushManager {
    NSString *deviceToken;
    NSString *firebaseDeviceToken;
    BOOL deviceTokenSended;
    BOOL deviceTokenListenerAdded;
    BOOL firebaseDeviceTokenSended;
    BOOL firebaseDeviceTokenListenerAdded;
}
RCT_EXPORT_MODULE();
- (BOOL)requiresMainQueueSetup {
    return YES;
}

- (NSArray<NSString *> *)supportedEvents {
  return @[nRNRPNDeviceTokenUpdated, nRNRPNFirebaseDeviceTokenUpdated, nRNRPNMessage];
}

+ (id)allocWithZone:(struct _NSZone *)zone {
    static RNRPushManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [super allocWithZone:zone];
    });
    return sharedMyManager;
}

- (void)registerForPushNotificationsWithApplication:(UIApplication *)application withYandexAPiKey:(NSString *)apiKey {
    [YMMYandexMetrica activateWithConfiguration:[[YMMYandexMetricaConfiguration alloc] initWithApiKey:apiKey]];
    [FIRApp configure];
    // Register for push notifications
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        if (NSClassFromString(@"UNUserNotificationCenter") != Nil) {
            // iOS 10.0 and above
            UNAuthorizationOptions options =
                UNAuthorizationOptionAlert |
                UNAuthorizationOptionBadge |
                UNAuthorizationOptionSound;
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            UNNotificationCategory *category = [UNNotificationCategory
                                                categoryWithIdentifier:@"Custom category"
                                                actions:@[]
                                                intentIdentifiers:@[]
                                                options:UNNotificationCategoryOptionCustomDismissAction];
            // Only for push notifications of this category dismiss action will be tracked.
            [center setNotificationCategories:[NSSet setWithObject:category]];
            [center requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError *error) {
                // Enable or disable features based on authorization.
            }];
        }
        else {
            // iOS 8 and iOS 9
            UIUserNotificationType userNotificationTypes =
                UIUserNotificationTypeAlert |
                UIUserNotificationTypeBadge |
                UIUserNotificationTypeSound;
            UIUserNotificationSettings *settings =
                [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
            [application registerUserNotificationSettings:settings];
        }
        [application registerForRemoteNotifications];
    }
    else {
        // iOS 7
        UIRemoteNotificationType notificationTypes =
            UIRemoteNotificationTypeBadge |
            UIRemoteNotificationTypeSound |
            UIRemoteNotificationTypeAlert;
        [application registerForRemoteNotificationTypes:notificationTypes];
    }
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result, NSError * _Nullable error) {
        firebaseDeviceToken = result.token;
        [self updateFirebaseDeviceToken];
    }];
}

- (void)didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
}

- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [YMPYandexMetricaPush handleRemoteNotification:userInfo];
    [self sendEventWithName:nRNRPNMessage body:userInfo];
}

- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [YMPYandexMetricaPush handleRemoteNotification:userInfo];
    [self sendEventWithName:nRNRPNMessage body:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)token {
    deviceToken = [[[[token description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    #ifdef DEBUG
        YMPYandexMetricaPushEnvironment pushEnvironment = YMPYandexMetricaPushEnvironmentDevelopment;
    #else
        YMPYandexMetricaPushEnvironment pushEnvironment = YMPYandexMetricaPushEnvironmentProduction;
    #endif
    [YMPYandexMetricaPush setDeviceTokenFromData:token pushEnvironment:pushEnvironment];
    [self updateDeviceToken];
}

- (void)updateDeviceToken {
    if (deviceTokenSended == NO && deviceTokenListenerAdded && deviceToken != nil) {
        [self sendEventWithName:nRNRPNDeviceTokenUpdated body:@{@"token": deviceToken}];
        deviceTokenSended = YES;
    }
}

- (void)updateFirebaseDeviceToken {
    if (firebaseDeviceTokenSended == NO && firebaseDeviceTokenListenerAdded && firebaseDeviceToken != nil) {
        [self sendEventWithName:nRNRPNFirebaseDeviceTokenUpdated body:@{@"token": firebaseDeviceToken}];
        firebaseDeviceTokenSended = YES;
    }
}

- (void)addListener:(NSString *)eventName {
    [super addListener:eventName];
    if ([eventName isEqualToString:nRNRPNDeviceTokenUpdated]) {
        deviceTokenListenerAdded = YES;
        [self updateDeviceToken];
    } else if ([eventName isEqualToString:nRNRPNFirebaseDeviceTokenUpdated]) {
        firebaseDeviceTokenListenerAdded = YES;
        [self updateFirebaseDeviceToken];
    }
}

@end
