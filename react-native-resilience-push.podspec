Pod::Spec.new do |s|
  s.name             = 'react-native-resilience-push'
  s.version          = '1.0.0'
  s.summary          = 'Firebase push component'

  s.description      = <<-DESC
    Firebase push component for react native
                       DESC

  s.homepage         = 'https://github.com/arturkh/react-native-resilience-push'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Resilience' => 'rresilience8@gmail.com' }
  s.source           = { :git => 'https://github.com/arturkh/react-native-resilience-push.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.source_files = 'ios/*.{h,m}'
  s.dependency 'React'
  s.dependency 'Firebase'
  s.dependency 'Firebase/Messaging'
  s.dependency 'YandexMobileMetricaPush'
  s.dependency 'YandexMobileMetrica'
end
